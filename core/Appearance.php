<?php
/**
 * Appearance
 *
 * Use this to create appearance for any model.
 */

namespace core;

trait Appearance
{
    /**
     * @var integer
     */
    private $eyes;
    /**
     * @var integer
     */
    private $ears;

    /**
     * Set eyes
     * @param integer $eyes
     */
    public function setEyes($eyes) { $this->eyes = $eyes; }

    /**
     * Get eyes
     * @return integer $eyes
     */
    public function getEyes() { return $this->eyes; }

    /**
     * Set ears
     * @param integer $ears
     */
    public function setEars($ears) { $this->ears = $ears; }

    /**
     * Get ears
     * @return integer $ears
     */
    public function getEars() { return $this->ears; }
}