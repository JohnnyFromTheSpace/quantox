<?php
/**
 * Female
 *
 * Use this class to create ordinary female(woman).
 */
namespace core;

include_once 'Human.php';

class Female extends Human
{
    private $gender = 'female';

    public function __construct($name, $surname, $age)
    {
        parent::__construct($name, $surname, $age, 2, 2, 1);
    }

    public function __toString()
    {
        return $this->getName() .' '. $this->getSurname();
    }

    /**
     * Get gender
     * @return string $gander
     */
    public function getGander() { return $this->gender; }

    public function introduce()
    {
        return sprintf(
            'Hi, I am <b>%s %s</b><br />'.
            'I am a <b>%s years</b> old<br />'.
            'I am represent of class <b>'. explode("\\", get_class($this))[1] .'</b><br />'.
            'I am a <b>%s</b><br />'.
            'I extends Human class',
            $this->getName(),
            $this->getSurname(),
            $this->getAge(),
            $this->getGander()
        );
    }
}