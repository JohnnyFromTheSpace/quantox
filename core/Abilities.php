<?php
/**
 * Abilities
 *
 * Use this to create abilities for any model.
 */

namespace core;

trait Abilities
{
    /**
     * @var boolean
     */
    private $speak;

    /**
     * @var boolean
     */
    private $walking;

    /**
     * Set speak
     * @param boolean $speak
     */
    public function setSpeak($speak) { $this->speak = $speak; }

    /**
     * Get speak
     * @return boolean $speak
     */
    public function getSpeak() { return $this->speak; }

    /**
     * Set walking
     * @param boolean $walking
     */
    public function setWalking($walking) { $this->walking = $walking; }

    /**
     * Get walking
     * @return boolean $walking
     */
    public function getWalking() { return $this->walking; }
}