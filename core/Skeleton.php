<?php
/**
 * Skeleton
 *
 * Use this to create base skeleton for any model.
 */

namespace core;

trait Skeleton
{
    /**
     * @var integer
     */
    private $heads;
    /**
     * @var integer
     */
    private $arms;
    /**
     * @var integer
     */
    private $legs;

    /**
     * Set heads
     * @param integer $heads
     */
    public function setHeads($heads) { $this->heads = $heads; }

    /**
     * Get heads
     * @return integer $heads
     */
    public function getHeads() { return $this->heads; }

    /**
     * Set arms
     * @param integer $arms
     */
    public function setArms($arms) { $this->arms = $arms; }

    /**
     * Get arms
     * @return integer $arms
     */
    public function getArms() { return $this->arms; }

    /**
     * Set legs
     * @param integer $legs
     */
    public function setLegs($legs) { $this->legs = $legs; }

    /**
     * Get legs
     * @return integer $legs
     */
    public function getLegs() { return $this->legs; }
}