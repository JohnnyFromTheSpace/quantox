<?php

require '../core/Human.php';
require '../core/Male.php';
require '../core/Female.php';

use core\Human;
use core\Male;
use core\Female;

$human = new Human();
$male = new Male('Johnny', 'Smith', 45);
$female = new Female('Olivia', 'Rose', 40);

echo $human->introduce();
include('human.html');
echo '<hr />';
echo $male->introduce() . '<br /><hr />';
echo $female->introduce() . '<br />';



