<?php
/**
 * Human
 *
 * Use this class to create ordinary human being.
 */

namespace core;

include_once 'Skeleton.php';
include_once 'Appearance.php';
include_once 'Abilities.php';

class Human
{
    use Skeleton, Appearance, Abilities;
    /**
     * @var string
     */
    private $name;
    /**
     * @var string
     */
    private $surname;
    /**
     * @var integer
     */
    private $age;

    public function __construct($name, $surname, $age)
    {
        $this->setSkeleton();
        $this->setAppearance();
        $this->setAbilities();
        $this->name    = $name;
        $this->surname = $surname;
        $this->age     = $age;
    }

    public function __toString()
    {
        return sprintf('Hi, I am a represent of humankind');
    }

    /**
     * Set name
     * @param string $name
     */
    public function setName($name) { $this->name = $name; }

    /**
     * Get name
     * @return string $name
     */
    public function getName() { return $this->name; }

    /**
     * Set surname
     * @param string $surname
     */
    public function setSurname($surname) { $this->surname = $surname; }

    /**
     * Get surname
     * @return string $surname
     */
    public function getSurname() { return $this->surname; }

    /**
     * Set age
     * @param integer $age
     */
    public function setAge($age) { $this->age = $age; }

    /**
     * Get age
     * @return integer $age
     */
    public function getAge() { return $this->age; }

    /**
     * Set skeleton.
     */
    private function setSkeleton()
    {
        $this->setHeads(1);
        $this->setArms(2);
        $this->setLegs(2);
    }

    /**
     * Set appearance.
     */
    private function setAppearance()
    {
        $this->setEyes(2);
        $this->setEars(2);
    }

    /**
     * Set abilities.
     */
    private function setAbilities()
    {
        $this->setSpeak(true);
        $this->setWalking(true);
    }

    public function introduce()
    {
        return sprintf(
            'Hi, I am a represent of <b>humankind</b>. <br />'.
            'I can have any name or surname it\'s doesn\'t matter.<br />'.
            'I have <b>%s head</b>, <b>%s arms</b> and <b>%s legs</b>.<br />'.
            'Also I have <b>%s eyes</b> and <b>%s ears</b><br />'.
            'I can <b>Speak</b> and <b>Walk</b><br />'.
            'I use Skeleton, Appearance and Abilities classes.',
            $this->getHeads(),
            $this->getArms(),
            $this->getLegs(),
            $this->getEyes(),
            $this->getEars()
        );
    }
}